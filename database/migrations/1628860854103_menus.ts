import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Menus extends BaseSchema {
  protected tableName = 'menus'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary();
      table.string('code').unique().notNullable();
      table.string('name').notNullable();
      table.integer('category');
      table.timestamp('created_at');
      table.timestamp('deleted_at');
      table.timestamp('modified_at');
      table.bigInteger('modified_by');
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
