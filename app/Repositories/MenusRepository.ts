import Menu from "App/Models/Menu";
import Database from "@ioc:Adonis/Lucid/Database";

export default class MenusRepository {

    public async insert(menu : Menu) {
        return await menu.save();
    }

    public async update(id: number, menu : Menu) {
        let menuDetail = await Menu.findBy("id", id);
        if(menuDetail == null) throw new Error("data not found");
        menuDetail.name = menu.name;
        menuDetail.price = menu.price;
        menuDetail.category = menu.category;

        return await menuDetail.save();
    }

    public async delete(id: number) {
        let menuDetail = await Menu.findByOrFail("id", id);
        menuDetail.delete();
    }

    public async findById(id: number) {
        return await Menu.findByOrFail("id", id);
    }

    public async getList(limit: number, offset: number, q: string) {
        return await Menu.query().where('name', 'ilike', `%${q}%`).orWhere('code', 'ilike', `%${q}%`)
                .limit(limit).offset(offset)
                .orderBy("id", "desc");
    }

    public async getAll(q: string) {
        const list = await Menu.all(); //query().where('name', 'ilike', `%${q}%`).orWhere('code', 'ilike', `%${q}%`)
        //.orderBy("id", "desc");
        return list;
    }

    public async count(q: string) {
        const total = await Database.from('menus').count('*').where('name', 'ilike', `%${q}%`).orWhere('code', 'ilike', `%${q}%`);
        return total[0].count;
    }



}
  