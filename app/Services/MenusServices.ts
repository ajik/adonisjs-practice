import Menu from "App/Models/Menu";
import MenusRepository from "App/Repositories/MenusRepository";
import { generateListResponse } from "App/Models/Response";

export default class MenusController {
    menuRepo = new MenusRepository();

    public async insertMenu(menu: Menu) {
        return await this.menuRepo.insert(menu);
    }

    public async updateMenu(id: number, menu: Menu) {
        return await this.menuRepo.update(id, menu);
    }

    public async deleteMenu(id: number) {
        return await this.menuRepo.delete(id);
    }

    public async getMenuById(id: number) {
        return await this.menuRepo.findById(id);
    }

    public async getList(limit: number, page: number, q: string) {
        let offset = (page - 1) * limit | 0;
        let totalData: number = await this.menuRepo.count(q);
        if(page > 0) {
            let list = await this.menuRepo.getList(limit, offset, q);
            let totalPage = totalData / offset;
            return generateListResponse(limit, page, totalData, totalPage, list);
        } else {
            let list = await this.menuRepo.getAll(q);
            console.log(list);
            return generateListResponse(0, 1, totalData, 1, list);
        }
    }
}