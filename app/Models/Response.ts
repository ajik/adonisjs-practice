const generateSuccessMessage = ({data}) => {
    return {
        code: 200,
        status: "success",
        message: "",
        data: data
    }
}

const generateErrorMessage = ({error}) => {
    return {
        code: 200,
        status: "error",
        message: error,
        data: null
    }
}

const generateListResponse = (limit, page, totalData, totalPage, list) => {
    return {
        limit: limit,
        page: page,
        totalData: totalData,
        totalPage: totalPage,
        list: list
    }
}

export {
    generateSuccessMessage,
    generateErrorMessage,
    generateListResponse
}