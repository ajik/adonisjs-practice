import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class Menu extends BaseModel {

  public static primaryKey = 'id';

  @column({isPrimary: true})
  public id: number;

  @column()
  public code: string;

  @column()
  public price: number;

  @column()
  public name: string;

  @column()
  public category: number;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: false })
  public deletedAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public modifiedAt: DateTime;

  @column()
  public modifiedBy: DateTime;
}
