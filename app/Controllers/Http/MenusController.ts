//import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import MenusServices from "App/Services/MenusServices";
import Menu from 'App/Models/Menu';
import { generateErrorMessage, generateSuccessMessage } from 'App/Models/Response';

export default class MenusController {

    menuService = new MenusServices();

    public async insertMenu({ request, response }) {
        let body = new Menu();
        body.name = request.input('name');
        body.code = request.input('code');
        body.category = request.input('category');
        body.price = request.input('price');
        
        try {
            const save = await this.menuService.insertMenu(body);
            if(save.$isPersisted) {
                response.send(generateSuccessMessage({data: save}));
            } else {
                response.send(generateErrorMessage({error: "error save data"}));
            }
        } catch(error) {
            console.log(error);
            response.send({error: error});
        }
    }

    public async updateMenu({params, request, response}) {
        let body = new Menu();
        body.name = request.input('name');
        body.category = request.input('category');
        body.price = request.input('price');
        const id: number = params.id;
        try {
            const save = await this.menuService.updateMenu(id, body);
            if(save.$isPersisted) {
                response.send(generateSuccessMessage({data: save}));
            } else {
                response.send(generateErrorMessage({error: "error save data"}));
            }
        } catch(error) {
            console.log(error);
            response.send({error: error});
        }
    }

    public async deleteMenu({params, response}) {
        try {
            const id: number = params.id;
            await this.menuService.deleteMenu(id);
            response.send(generateSuccessMessage({data: null}));
        } catch (error) {
            console.log(error);
            response.send({error: error});
        }
    }

    public async getMenuById({params, response}) {
        try {
            const id: number = params.id;
            const menu = await this.menuService.getMenuById(id);
            response.send(generateSuccessMessage({data: menu}));
        } catch (error) {
            console.log(error);
            response.send({error: error});
        }
    }

    public async indexMenu({request, response}) {
        try {
            const limit: number = request.input('limit');
            const page: number = request.input('page');
            const q: string = request.input('q');
            const list = await this.menuService.getList(limit, page, q);
            response.send(generateSuccessMessage({data: list}));
        } catch (error) {
            console.log(error);
            response.send({error: error});
        }
    }
}
