create table if not exists menus (
    id bigint not null unique,
    code varchar(10) not null unique,
    name varchar(255) not null,
    price decimal default null,
    category integer default null,
    created_at timestamp default now(),
    modified_at timestamp default null,
    deleted_at timestamp default null,
    modified_by bigint default null
)

CREATE SEQUENCE IF NOT EXISTS menus_id_seq INCREMENT 1 START 1;